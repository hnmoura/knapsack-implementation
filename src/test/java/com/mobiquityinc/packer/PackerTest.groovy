package com.mobiquityinc.packer

import com.mobiquityinc.exception.APIException
import spock.lang.Specification
import spock.lang.Subject

class PackerTest extends Specification {
    def 'Should read file and return the expected values'() {
        given:
        String path = "src/test/resources/CorrectInput.txt"

        @Subject
        Packer packer = new Packer()

        when:
        final String output = packer.pack(path)

        then:
        output == '4\n-\n2,7\n8,9'

    }

    def 'Should read file and return an exception of wrong path'() {
        given:
        String path = 'src/test/resource/CorrectInput.txt'

        @Subject
        Packer packer = new Packer()

        when:
        packer.pack(path)

        then:
        thrown APIException
    }

    def 'Should read file and return an exception of wrong knapsack weight'() {
        given:
        String path = 'src/test/resources/WrongKnapsackWeightInput.txt'

        @Subject
        Packer packer = new Packer()

        when:
        packer.pack(path)

        then:
        APIException apiException = thrown()
        apiException.message == 'The box max weight must be equals or less than 100 weight units'
    }

    def 'Should read file and return an exception of more the 15 item on a line'() {
        given:
        String path = 'src/test/resources/MoreThan15Items.txt'

        @Subject
        Packer packer = new Packer()

        when:
        packer.pack(path)

        then:
        APIException apiException = thrown()
        apiException.message == 'The following rules are violated on Box: \n' +
                'items: A box can not be empty or have more than 15 items.'
    }


}
