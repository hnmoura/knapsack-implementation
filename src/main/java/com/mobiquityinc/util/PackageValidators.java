package com.mobiquityinc.util;

import com.mobiquityinc.domainobject.Item;
import com.mobiquityinc.domainobject.Box;
import com.mobiquityinc.exception.APIException;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import java.util.Set;
import java.util.stream.Collectors;

import static com.mobiquityinc.domainvalue.PackagingConstraints.MAX_WEIGHT_BOX;

final class PackageValidators {

    private static final Validator VALIDATOR = Validation.buildDefaultValidatorFactory().getValidator();

    private static final PackageValidators PACKAGE_VALIDATORS = new PackageValidators();

    private PackageValidators() {
    }

    static PackageValidators getInstance() {
        return PACKAGE_VALIDATORS;
    }

    double getBoxWeightFromString(final String[] splitStringBox) throws APIException {
        if (splitStringBox.length != 2) {
            throw new APIException("The split string should have 2 parts (Weight and a list of Items");
        }
        try {
            int maxWeight = Integer.parseInt(splitStringBox[0].trim());
            if (maxWeight > MAX_WEIGHT_BOX) {
                throw new APIException("The box max weight must be equals or less than 100 weight units");
            }

            return maxWeight;
        } catch (NumberFormatException e) {
            throw new APIException("The first element on the string must be a valid number");
        }
    }

    boolean isAValidItem(final Item item) throws APIException {
        Set<ConstraintViolation<Item>> violations = VALIDATOR.validate(item);
        if (violations.isEmpty()) {
            return true;
        }
        Set<String> violationMessages = violations.stream().map(
                constraintViolation -> constraintViolation.getPropertyPath() + ": "
                        + constraintViolation.getMessage()).collect(Collectors.toSet());

        throw new APIException("The following rules are violated on Item: \n" + String.join("\n", violationMessages));

    }

    boolean isAValidBox(final Box box) throws APIException {
        Set<ConstraintViolation<Box>> violations = VALIDATOR.validate(box);
        if (violations.isEmpty()) {
            return true;
        }
        Set<String> violationMessages = violations.stream().map(
                constraintViolation -> constraintViolation.getPropertyPath() + ": "
                        + constraintViolation.getMessage()).collect(Collectors.toSet());

        throw new APIException("The following rules are violated on Box: \n" + String.join("\n", violationMessages));

    }

}
