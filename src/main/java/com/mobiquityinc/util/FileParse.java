package com.mobiquityinc.util;

import com.mobiquityinc.domainobject.Item;
import com.mobiquityinc.domainobject.Box;
import com.mobiquityinc.exception.APIException;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class FileParse {

    private static final String INDEX = "index";

    private static final String WEIGHT = "weight";

    private static final String COST = "cost";

    private static final String BOX_REGEX = "\\((?<" + INDEX + ">\\d+),(?<" + WEIGHT + ">\\d+(\\.\\d{1,2})?),€(?<" + COST + ">\\d+(\\.\\d{1,2})?)\\)";

    private static final Character SPACE = 0x20;

    private static final FileParse FILE_PARSE = new FileParse();

    private final PackageValidators packageValidators = PackageValidators.getInstance();

    private FileParse() {
    }

    public static FileParse getInstance() {
        return FILE_PARSE;
    }

    public List<Box> parseFileToBoxOfItems(final String filePath) throws APIException {
        List<Box> boxes = new ArrayList<>();

        File fileDir = new File(filePath);

        try (BufferedReader in = new BufferedReader(
                new InputStreamReader(
                        new FileInputStream(fileDir), StandardCharsets.UTF_8))) {

            String boxAsString = in.readLine();
            while (Objects.nonNull(boxAsString)) {
                Box box = parseStringToBox(boxAsString);
                if (packageValidators.isAValidBox(box)) {
                    boxes.add(box);
                }
                boxAsString = in.readLine();
            }

        } catch (IOException e) {
            throw new APIException("Error while trying to read the file, please check the path and try again", e);
        }

        return boxes;
    }

    private Box parseStringToBox(final String boxAsString) throws APIException {
        String[] splitStringBox = boxAsString.trim().split(":");

        double maxBoxWeight = packageValidators.getBoxWeightFromString(splitStringBox);
        int lastPosition = 0;
        Pattern pattern = Pattern.compile(BOX_REGEX);
        Matcher matcher = pattern.matcher(splitStringBox[1]);
        List<Item> itemList = new ArrayList<>();

        while (matcher.find()) {
            if (matcher.start() != lastPosition + 1 || splitStringBox[1].charAt(lastPosition) != SPACE) {
                throw new APIException(String.format("The string structure does not match with the template (%s)"
                        + " model expected on this project", BOX_REGEX));
            }
            try {
                int index = Integer.parseInt(matcher.group(INDEX));
                double weight = Double.parseDouble(matcher.group(WEIGHT));
                double cost = Double.parseDouble(matcher.group(COST));

                Item item = new Item(index, weight, cost);
                if (packageValidators.isAValidItem(item)) {
                    itemList.add(item);
                }

            } catch (ArithmeticException e) {
                throw new APIException("Error during parse the string to number", e);
            }
            lastPosition = matcher.end();
        }

        return new Box(maxBoxWeight, itemList);

    }

}
