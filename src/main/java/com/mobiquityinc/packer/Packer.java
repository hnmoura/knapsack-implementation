package com.mobiquityinc.packer;

import com.mobiquityinc.domainobject.Box;
import com.mobiquityinc.exception.APIException;
import com.mobiquityinc.service.PackageService;
import com.mobiquityinc.util.FileParse;

import java.util.List;
import java.util.stream.Collectors;

public class Packer {

    public static String pack(final String filePath) throws APIException {
      FileParse fileParse = FileParse.getInstance();
      PackageService packageService = PackageService.getInstance();

      List<Box> boxList = fileParse.parseFileToBoxOfItems(filePath);
        return boxList.stream().map(packageService::calculateTheBestCombination)
                .collect(Collectors.joining("\n"));
    }
}
