package com.mobiquityinc.domainobject;

import lombok.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import static com.mobiquityinc.domainvalue.PackagingConstraints.*;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
public class Item {

    @NotNull(message = "The index property can not be null.")
    private int index;

    @NotNull(message = "The weight property can not be null.")
    @Max(value = MAX_WEIGHT_ITEM, message = "The item can not be heavier than 100 weight units.")
    @Min(value = MIN_WEIGHT, message = "The item must be heavier than 0.")
    private double weight;

    @NotNull(message = "The cost property can not be null.")
    @Max(value = MAX_COST_ITEM, message = "The item can not be more expensive than 100 euros.")
    @Min(value = MIN_COST, message = "The item must be expensive than 0.")
    private double cost;

    public int getWeight() {
        return (int) weight * 100;
    }
}
