package com.mobiquityinc.domainobject;

import lombok.*;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

import static com.mobiquityinc.domainvalue.PackagingConstraints.*;

@Setter
@Getter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
public class Box {

    @NotNull(message = "The maxWeight property can not be null.")
    @Max(value = MAX_WEIGHT_BOX, message = "Weight: ${validatedValue} is not a valid weight for the package box. "
            + "The The max weight of knapsack can not be heavier than 100 weight units.")
    @Min(value = MIN_WEIGHT, message = "Weight: ${validatedValue} is not a valid weight for the package box. "
            + "The min weight of the box must be heavier than 0.")
    private double weight;

    @NotNull(message = "The property items can not be null.")
    @Size(min = MIN_NUMBER_OF_ITEMS, max = MAX_NUMBER_OF_ITEMS,
            message = "A box can not be empty or have more than 15 items.")
    private List<Item> items;

    public int getWeight() {
        return (int) weight * 100;
    }

}
