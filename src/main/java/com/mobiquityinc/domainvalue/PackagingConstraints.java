package com.mobiquityinc.domainvalue;

public final class PackagingConstraints {

    private PackagingConstraints() {
    }

    public static final int MAX_WEIGHT_BOX = 100;

    public static final int MAX_WEIGHT_ITEM = 100;

    public static final int MIN_WEIGHT = 0;

    public static final int MAX_COST_ITEM = 100;

    public static final int MIN_COST = 0;

    public static final int MAX_NUMBER_OF_ITEMS = 15;

    public static final int MIN_NUMBER_OF_ITEMS = 1;

}
