package com.mobiquityinc.service;

import com.mobiquityinc.domainobject.Item;
import com.mobiquityinc.domainobject.Box;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class PackageService {

    private static final PackageService PACKAGE_SERVICE = new PackageService();

    private PackageService() {
    }

    public static PackageService getInstance() {
        return PACKAGE_SERVICE;
    }

    public String calculateTheBestCombination(final Box box) {
        int numberOfItems = box.getItems().size() + 1;
        int boxWeightCapacity = box.getWeight() + 1;
        // We use a matrix to store the max value ot each n-th item
        double[][] a = new double[numberOfItems][boxWeightCapacity];

        // we iterate on items
        for (int i = 1; i < numberOfItems; i++) {
            Item item = box.getItems().get(i - 1);
            //We iterate on each capacity
            for (int j = 1; j < boxWeightCapacity; j++) {
                if (item.getWeight() > j) {
                    a[i][j] = a[i - 1][j];
                } else {
                    // we maximize value at this rank in the matrix
                    a[i][j] = Math.max(a[i - 1][j], a[i - 1][(j - item.getWeight())] + item.getCost());
                }
            }
        }

        List<Integer> indexes = new ArrayList<>();
        int j = box.getWeight();
        double totalCost = a[numberOfItems - 1][boxWeightCapacity - 1];

        while (j > 0 && a[numberOfItems - 1][j - 1] == totalCost) {
            j--;
        }

        for (int i = numberOfItems - 1; i > 0; i--) {
            if (a[i][j] != a[i - 1][j]) {
                indexes.add(box.getItems().get(i - 1).getIndex());
                // we remove items value and weight
                j -= box.getItems().get(i - 1).getWeight();
            }
        }

        String result = indexes.stream()
                .mapToInt(i -> i)
                .sorted()
                .mapToObj(Integer::toString)
                .collect(Collectors.joining(","));
        return result.isEmpty() ? "-" : result;
    }
}
